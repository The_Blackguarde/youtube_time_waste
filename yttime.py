import youtube_dl
import json
import sys
import datetime


class MyLogger(object):
    def debug(self, msg):
        print(msg) 
        pass 
    
    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)

class yt(object):

    def __init__(self):
        super().__init__()
        self.ydl_opts = {}

    def add_links(self, link, number):
        if number == -1:
            ydl_opts = {'logger': MyLogger(),'ignoreerrors':True, "default_search" : 'auto'}
        else:
            ydl_opts = {'logger': MyLogger(),'ignoreerrors':True, "default_search" : 'auto', "playlistend" : number}
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            info_dict = ydl.extract_info(link, download=False)
            strinput = json.dumps(info_dict)
            jsondata = json.loads(strinput)
            time = 0
            if 'entries' in jsondata:
                for row in jsondata['entries']:
                    if row != None:
                        time += row['duration']
            else:
                time = jsondata['duration']
            print(str(time) + " sec")
            print(str(datetime.timedelta(seconds=time)))
            
        return

youtube=yt()
if len(sys.argv) < 3: #should use argparse
    if len(sys.argv) < 2:
        sys.exit("No command line arguments found")
    else:
        arg2=-1
else:
    arg2=int(sys.argv[2])
youtube.add_links(sys.argv[1], arg2)

