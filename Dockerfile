FROM python:3.7-alpine

RUN pip install youtube_dl

COPY . /youtube

WORKDIR /youtube

ENTRYPOINT ["python", "yttime.py"]

#CMD ["yttime.py"]


